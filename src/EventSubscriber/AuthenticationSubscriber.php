<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\Event\CheckPassportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;

class AuthenticationSubscriber implements EventSubscriberInterface
{
    private $userRepository;
    private $mailer;

    public function __construct(
        UserRepository $userRepository,
        MailerInterface $mailer
    )
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onUserFirstConnexion',
        ];
    }

    public function getRequest(LoginSuccessEvent $event) : Request
    {
        return $event->getRequest();
    }

    public function getUser(LoginSuccessEvent $event) :User
    {
        $request = $this->getRequest($event);
        $userName = $request->request->get('name');

        return $this->userRepository->findOneBy(['name' => $userName]);
    }

     public function getUserEmail(LoginSuccessEvent $event) :string
    {
        $user = $this->getUser($event);

        return $user->getEmail();
    }

    public function getUserBrowser(LoginSuccessEvent $event) :?string
    {
        $user = $this->getUser($event);

        return $user->getBrowser();
    }

    public function getUserIpAddress(LoginSuccessEvent $event) :?string
    {
        $user = $this->getUser($event);

        return $user->getIpAddress();
    }

    public function onUserFirstConnexion(LoginSuccessEvent $event)
    {
        $request = $this->getRequest($event);
        $user = $this->getUser($event);
        $userIpAddress = $this->getUserIpAddress($event);
        $userBrowser = $this->getUserBrowser($event);


        if(null === $userIpAddress && null === $userBrowser){
            $user->setIpAddress($request->getClientIp());
            $user->setBrowser($request->headers->get('user-agent'));
        }
    }

    // Pour navigateur :
    public function onCheckUserBrowser(LoginSuccessEvent $event)
    {
        // !== browser dans la bdd  ---> bloque la connexion + mailer envoyera mail pour confirmation de connexion

        $request = $this->getRequest($event);
        $userBrowser = $this->getUserBrowser($event);
        if($request->headers->get('user-agent') !== $userBrowser){
            $userMail = $this->getRequest($event)->request->get('email');
            $email = (new Email())
                ->from('no-reply@epsi.fr')
                ->to($userMail)

                ->subject('Confirmation de connexion')
                ->text('Cliquez sur le lien pour connecter');
//                ->html('<a href=>Votre location actuel est : ' . $request->getClientIp() .'</a>'
//                );
            $this->mailer->send($email);
        }

    }

    // Pour Addresse IP :
    public function onCheckUserIpAddress(LoginSuccessEvent $event)
    {
        // ! == ip dans bdd ---> envoyer un mail
        // si ip !== France ----> bloque la connexion

        $request = $this->getRequest($event);
        $userIpAddress = $this->getUserIpAddress($event);

        if($request->getClientIp() !== $userIpAddress){
            $userMail = $this->getRequest($event)->request->get('email');
            $email = (new Email())
                ->from('no-reply@epsi.fr')
                ->to($userMail)

                ->subject('Confirmation de connexion')
                ->text('Sending emails is fun again!')
                ->html('<p>Votre location actuel est : ' . $request->getClientIp() .'</p>'
                );
            $this->mailer->send($email);
        }
    }
//
//    public function sendEmailForIp(MailerInterface $mailer, LoginSuccessEvent $event): Response
//    {
//        $userMail = $this->getRequest($event)->request->get('email');
//        $email = (new Email())
//            ->from('no-reply@epsi.fr')
//            ->to($userMail)
//
//            ->subject('Confirmation de connexion')
//            ->text('Sending emails is fun again!')
//            ->html('<p>See Twig integration for better HTML integration!</p>');
//
//        $mailer->send($email);
//
//        // ...
//    }
}
